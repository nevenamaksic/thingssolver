FROM php:8.0-apache-buster

WORKDIR /var/www/html

COPY . /var/www/html/

EXPOSE 14450

CMD ["apache2-foreground"]
